## Examen unidad 6,7 y 8
Cree un programa que imprima la serie producida por la conjetura de Collatz, imprimiendo los numeros de *n* hasta el 1. Utilice funciones.

Ejemplo
```bash
Entrada
n = 4
salida
2 1
Entrada
n = 8
salida
4 2 1
Entrada
n = 6
salida
3 10 5 16 8 4 2 1
```
Tome en cuenta lo siguiente:
- La conjetura de Collatz se explica en el siguiente video:
https://www.youtube.com/watch?v=HpcYW08Ug7g


## Condiciones de entrega
Subira el ejecutable (.exe) a su cuenta de github, el cual tendra lo siguiente

nombre: unidad6[numerodecontrol].exe,

ejemplo: unidad612161219.exe

ademas del codigo.

