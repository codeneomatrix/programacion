## Examen unidad 2,3,4 y 5
Cree un programa que entrege el *n-esimo* numero de la serie de Fibonacci, el valor lo ingresara por teclado.El valor de *n* estara en el rango del 1 al 10.

Ejemplo
```bash
Entrada
n = 3
salida
2
Entrada
n = 9
salida
34
Entrada
n = 2
salida
1
```
Tome en cuenta lo siguiente:
- La sucesión de Fibonacci se explica en el siguiente video:
https://www.youtube.com/watch?v=yDyMSliKsxI

- El numero *n* es un numero cualquiera entre 1 y 10 que introducira por teclado.


## Condiciones de entrega
Subira el ejecutable (.exe) a su cuenta de github, el cual tendra lo siguiente

nombre: unidad2[numerodecontrol].exe,

ejemplo: unidad212161219.exe

ademas del codigo.


