## Reto 1
Cree un diagrama de flujo, que imprima la serie producida por la conjetura de Collatz, imprimiendo los numeros de *n* hasta el 1. 
Tome en cuenta lo siguiente:
- La conjetura de Collatz se explica en el siguiente video:
https://www.youtube.com/watch?v=HpcYW08Ug7g
- El numero *n* es cualquier numero mayor a 1. 

### Condiciones de entrega
Entregara impreso o a mano su diagrama de flujo.

## Reto 2
Los numero felices son aquellos que al multiplicar las cifras que los conforman y sumar esos productos, despues de *n* iteraciones el resulatdo sera 1.
Dado un numero cualquiera (del 10 hasta el 44) introducido por teclado, imprimir en pantalla la serie de numeros que genera este procedimento incluyendo el 1 (si es que llega hasta el uno). 

Tome en cuenta lo siguiente :
- Los numeros felices se explican en el siguiente video:
https://www.youtube.com/watch?v=R2D8SuRjtnQ
- El numero *n* es un numero entre 10 y 44 , que se introducira por teclado
- Puede separar un numero por ejemplo el 23 de la siguente manera :
      23/10= 2  (division)
      23%10= 3  (modulo)
- 
Ejemplo:
``` bash
~/Desktop $ ./reto2 
23
23 13 10 01 01 
~/Desktop $ ./reto2
44
44 32 13 10 01  
```
### Condiciones de entrega
Subira el ejecutable (.exe) a su cuenta de github, el cual tendra lo siguiente

nombre: reto2[numerodecontrol].exe,

ejemplo: reto212161219.exe

ademas del codigo.


## Reto 3
Cree un programa que entrege el *n-esimo* numero de la serie de Fibonacci, el valor lo ingresara por teclado.El valor de *n* estara en el rango del 1 al 10. UTILIZANDO FUNCIONES EN C. 

Ejemplo
```bash
Entrada
n = 3
salida
2
Entrada
n = 9
salida
34
Entrada
n = 2
salida
1
```
Tome en cuenta lo siguiente:
- La sucesión de Fibonacci se explica en el siguiente video:
https://www.youtube.com/watch?v=yDyMSliKsxI

- El numero *n* es un numero cualquiera entre 1 y 10 que introducira por teclado.


### Condiciones de entrega
Subira el ejecutable (.exe) a su cuenta de github, el cual tendra lo siguiente

nombre: reto3[numerodecontrol].exe,

ejemplo: reto312161219.exe

ademas del codigo.

